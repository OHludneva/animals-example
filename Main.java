package animalsExample;

public class Main {
    public static void main(String[] args) {
        Animal eagle = new Animal("Eagle", 5, "flying");
        Animal chicken = new Animal("Chicken", 2, "running");
        Animal crocodile = new Animal("Crocodile", 100, "walking");
        Animal snake = new Animal("Snake", 35, "crawling");

        System.out.println(eagle);
        System.out.println(chicken);
        System.out.println(crocodile);
        System.out.println(snake);

        Animal owl = new Birds ("Owl", 5, "flying", "black");
        Animal turtle = new Reptiles ("Turtle", 150, "walking", 70);

        System.out.println(owl);
        System.out.println(turtle);
    }

}

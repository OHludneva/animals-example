package animalsExample;

public class Animal {

    public String name;
    public Integer weight;
    public String movementType;

    public Animal(String name, Integer weight, String movementType){
        this.name = name;
        this.weight = weight;
        this.movementType = movementType;
        System.out.println("All animals possess 3 main characteristics");
    }

    @Override
    public String toString() {
        return "Animal: " +
                "name = " + name +
                ", weight = " + weight +
                ", movementType = " + movementType;
    }
}

package animalsExample;

public class Reptiles extends Animal {

    private Integer lifespan;

    public Reptiles(String name, Integer weight, String movementType, Integer lifespan) {
        super(name, weight, movementType);
        this.lifespan = lifespan;
    }

    public Integer getLifespan() {
        return lifespan;
    }

    @Override
    public String toString() {
        return super.toString() +
                ", lifespan = " + lifespan;
    }
}

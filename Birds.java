package animalsExample;

public class Birds extends Animal {

    private String colour;

    public Birds(String name, Integer weight, String movementType, String colour) {
        super(name, weight, movementType);
        this.colour = colour;
    }

    public String getColour() {
        return colour;
    }

    @Override
    public String toString() {
        return super.toString() +
                ", colour = " + colour;
    }
}
